package com.TennisKata.core;



import java.util.Random;

public class Game {


    private Player player1,player2;
    //default points
    private  final String[] scores = new String[]{"Love", "Fifteen", "Thirty", "Forty"};


    public Game(String playerOneName, String playerTwoName) {
        player1=new Player(playerOneName,0);
        player2=new Player(playerTwoName,0);
          }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public void start() {
        do { //select player
            System.out.println("Point " + (this.randomPoint() == 1 ? player1.getName() : player2.getName()));
            //calculate score
            System.out.println("Score: " + calculateScore());
        }
        //play until one of players win
        while (!calculateScore().startsWith("win"));
    }

    public Integer randomPoint() {
        // select a random player
        Integer userId =new Random().nextInt(2) + 1;
        //set score for the player
        if(userId==1)
            player1.setScore(player1.getScore()+1);
        else
            player2.setScore(player2.getScore()+1);


        return userId;
    }




    public  String calculateScore(){
        //Deuce
        if(player1.getScore().equals( player2.getScore())) return deuce();
        //win or advantage
        if(player1.getScore() > 3 || player2.getScore() > 3) return winOrAdvantage();
        return defaultScore();
    }

    private  String deuce() {
        if(player1.getScore() < 3) return scores[player1.getScore()]  + " All";
        // least three points have been scored by each player, and the scores are equal, the score is deuce
        return "Deuce";
    }

    private  String winOrAdvantage() {
        // a player  have won at least four points in total and at least two points more than the opponents
        if(Math.abs(player1.getScore() - player2.getScore()) >= 2) return win();
        return advantage();
    }

    private  String win() {
        if(player1.getScore()>player2.getScore()) return ("win for "+ player1.getName());
        return ("win for "+ player2.getName());
    }

    private  String advantage() {
        // at least three points have been scored by each side and a player has one more point than his opponent, the score of the game is “advantage” for the player in the lead.
        if(player1.getScore()>player2.getScore()) return ("Advantage "+ player1.getName());
        return ("Advantage "+ player2.getName());
    }
    //  score of the game
    private  String defaultScore() {
        return scores[player1.getScore()] + " - " + scores[player2.getScore()];
    }
}

