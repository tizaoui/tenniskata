package com.TennisKata.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameTest {
    Game game = new Game("yann" , "danniel");

    @Test
    public void testPlayer1WinsFirstBall()
    {
        game.getPlayer1().setScore(1);

        String score = game.calculateScore();
        assertEquals("Fifteen - Love", score);
    }

    @Test
    public void testFifteenAll(){
        game.getPlayer1().setScore(1);
        game.getPlayer2().setScore(1);

        String score = game.calculateScore();
        assertEquals("Fifteen All", score);
    }


    @Test
    public void testPlayer1WinsFirstThreeBalls(){
        game.getPlayer1().setScore(3);
        game.getPlayer2().setScore(0);
        String score = game.calculateScore();
        assertEquals("Forty - Love", score);
    }
    @Test
    public void testPlayersAreDeuce3() {
        game.getPlayer1().setScore(3);
        game.getPlayer2().setScore(3);

        String score = game.calculateScore();
        assertEquals("Deuce", score);
    }


    @Test
    public void testPlayer2Wins(){
        game.getPlayer1().setScore(1);
        game.getPlayer2().setScore(4);

        String score = game.calculateScore();
        assertEquals("win for danniel", score);
    }
    @Test
    public void testDeuce4() {
        game.getPlayer1().setScore(4);
        game.getPlayer2().setScore(4);
        String score = game.calculateScore();
        assertEquals("Deuce", score);
    }

    @Test
    public void testPlayer1Advantage(){
        game.getPlayer1().setScore(6);
        game.getPlayer2().setScore(5);

        String score = game.calculateScore();
        assertEquals("Advantage yann", score);
    }


    @Test
    public void testPlayer2WinsAfterAdvantage() {
        game.getPlayer1().setScore(8);
        game.getPlayer2().setScore(10);
        String score = game.calculateScore();
        assertEquals("win for danniel", score);
    }



}
